// Main page

// ----------------------
// IMPORTS

/* NPM */
import React from 'react';
import { Switch, Route } from 'react-router-dom';
// Use the Semantic UI React framework (https://react.semantic-ui.com)
// to get some nicely styled components

/* App */

// Components
import Vockice from '../Vockice';

export default () => (
  <Switch>
    <Route path="/" component={Vockice} /> 
  </Switch>
)