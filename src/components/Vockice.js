import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import styles from './styles/styles.css'
import { confirmAlert } from 'react-confirm-alert';


@connect(state => ({ counter: state.counter }))
class Vockice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posudaZaVoce: [],
      kapacitetPosude: 0,
      boja: '',
      ostalo: 1,
      greska: '',
      sveJabuke: '',
      sokovnikZapremina: 20,
      iscedjeno: 0,
      kapacitetGreska: ''
    }
  }
  kapacitetPosude = (e) => {
    if (e.target.name == 'kapacitet') {
      this.setState({
        kapacitetPosude: e.target.value
      })
    } else {
      this.setState({
        boja: e.target.value
      })
    }
  }
  dodajJabuku = () => {
    let posudaZaVoce = this.state.posudaZaVoce;
    let jabuke = this.state.sveJabuke
    let posuda = []
    let sveJabuke = []
    let crvljiva = Math.random() < 0.2;
    let zapremina = Math.floor(Math.random() * 5) + 1
    let jabuka = {
      boja: this.state.boja,
      zapremina,
      crvljiva
    }
    if (!jabuka.crvljiva) {
      posuda.push(jabuka)
      if (this.state.ostalo > 0 && this.state.kapacitetPosude > 0) {
        let noviNiz = [...posudaZaVoce, ...posuda]
        this.setState({
          posudaZaVoce: noviNiz,
          ostalo: this.state.kapacitetPosude - noviNiz.length,
          greska: ''
        })
      } else {
        this.setState({
          greska: 'Kapacitet posude je ispunjen'
        })
      }
    }
    if (this.state.ostalo > 0 && this.state.kapacitetPosude > 0) {
      sveJabuke.push(jabuka)
      this.setState({
        sveJabuke: [...jabuke, ...sveJabuke]
      })
    }
  }
  iscediSok = () => {
    let rez = this.state.posudaZaVoce.reduce((a, b) => ({zapremina: a.zapremina + b.zapremina}))
    this.setState({
      iscedjeno: rez.zapremina / 2
    })
    if(rez.zapremina / 2 > this.state.sokovnikZapremina){
      this.setState({
        kapacitetGreska: 'Premasen kapacitet'
      })
    }
  }
  render() {
    return (
      <div>
        <span><br />
          Broj vocki:
          {this.state.posudaZaVoce.length}
        </span><br />
        <span>
          Preostalo mesta:
          {this.state.kapacitetPosude - this.state.posudaZaVoce.length}
        </span>
        <br />
        <label>
          Unesite Kapacitet sokovnika
        </label> <br />
        <input value={this.state.kapacitetPosude} type='number' name='kapacitet' onChange={this.kapacitetPosude} /> <br />
        <label>
          Boja jabuke
        </label> <br />
        <input value={this.state.boja} name='boja' onChange={this.kapacitetPosude} />
        <button onClick={this.dodajJabuku}> Dodaj jabuku </button> <br />
        <span style={{ color: 'red' }}> {this.state.greska} </span> <br />
        <span> Sve jabuke </span> <br />
        <table>
          <thead>
            <tr>
              <th>Boja</th>
              <th>Zapremina</th>
              <th>Crvljiva</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.sveJabuke.length ?
                this.state.sveJabuke.map(item => {
                  let crvljiva = item.crvljiva ? 'crvljiva' : 'nije crvljiva'
                  return (
                    <tr>
                      <td>{item.boja}</td>
                      <td>{item.zapremina}</td>
                      <td>{crvljiva}</td>
                    </tr>
                  )
                }) : null
            }
          </tbody>
        </table>
        <div>
          <button onClick={this.iscediSok}> Iscedi sok </button>
          <p> Kapacitet: {this.state.sokovnikZapremina}l </p>
          <p> Iscedjeno: {this.state.iscedjeno}l </p>
          <span style={{ color: 'red' }}> {this.state.kapacitetGreska} </span>
        </div>
      </div>
    );
  }
}

export default withRouter(Vockice);